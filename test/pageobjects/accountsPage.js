import Page from './page';
import utl   from '../../utilities/common-utilities';
var cjson = require('cjson');
var accountData = cjson.load(process.cwd() + '/testData/accountData.json');
class AccountsPage extends Page {

    

    createAccount(data){
        var accountValues = accountData[data]
        var accountName = "Account "+Math.floor(new Date() / 1000);
        this.button('New').click();
        this.accountTypeRadio(data).click();
        this.nextButton.click();
        this.textBox('Account Name').setValue(accountName);
        this.selectDropdown('Status',accountValues.Status);
        this.saveButton.click();

    }
    
}

export default new AccountsPage()