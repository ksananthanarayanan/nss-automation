

export default class Page {

//***************************** Common Elements and Methods ************************************/

button(data){

  return $(`//a[@title='${data}']`);
}
accountTypeRadio(data){
  return $(`//*[contains(text(),'New Account')]/following::span[contains(text(),'${data}')]`)
}
get nextButton(){
  return $("//button[@data-aura-class='uiButton']/span[contains(text(),'Next')]");
}
get saveButton(){
  return $("//button[@title='Save']");
}
textBox(data){
  return $(`(//h2[contains(text(),'New Account')]/following::span[contains(text(),'${data}')]/following::input)[1]`)
}
selectDropdown(field,data){
  $(`(//h2[contains(text(),'New Account')]/following::span[contains(text(),'${field}')]/following::a)[1]`).click();
  browser.pause(3000);
  $(`(//h2[contains(text(),'New Account')]/following::span[contains(text(),'Status')]/following::a[@title='${data}'])[1]`).click();
}


  open (path) {
    browser.url(path)
  }
}
