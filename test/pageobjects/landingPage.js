import Page from './page';
import utl   from '../../utilities/common-utilities';
var cjson = require('cjson');
var loginData = cjson.load(process.cwd() + '/testData/SF_Login_Creds.json');
class LandingPage extends Page {

    
    objectName(tabName)   { return $(`//a[@title='${tabName}']`); }   
   
    
}

export default new LandingPage()
