import Page from './page';
import utl   from '../../utilities/common-utilities';


var cjson = require('cjson');
var loginData = cjson.load(process.cwd() + '/testData/SF_Login_Creds.json');
class LoginPage extends Page {

    
    get usernameInput()   { return $("//input[@id='username']"); }
    get passwordInput()   { return $("//input[@id='password']"); }
    get loginButton()     { return $("//input[@id='Login']"); }
    get logOutImage()     { return $("//span[@class='photoContainer forceSocialPhoto']");}
    get logOutButton()    { return $("//a[contains(text(),'Log Out')]");}

    
   
    login (data) {
      var loginValues = loginData[data];
      this.usernameInput.setValue(loginValues.userId);
      this.passwordInput.setValue(loginValues.password);
      this.loginButton.click();
    }
    logout(){
      browser.pause(3000);
      browser.refresh();
      this.logOutImage.waitForDisplayed(35000);
      this.logOutImage.click();
      this.logOutButton.waitForDisplayed(35000);
      this.logOutButton.click();
    }
}

export default new LoginPage()
