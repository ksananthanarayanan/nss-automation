const { Given } = require('@cucumber/cucumber')

Given('I am on the SalesForce login page', async function () {
  // Write code here that turns the phrase above into concrete actions
  await browser.url("https://centricanss--test.my.salesforce.com/");  // navigating to login page
});
