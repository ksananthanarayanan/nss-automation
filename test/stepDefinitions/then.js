const { Then } = require('@cucumber/cucumber')

Then(/^The account should be created successfully$/, function () {
  var ele = $("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
  ele.waitForDisplayed(35000)
  if(ele.isExisting()){
    console.log("Account is created successfully!!!")
    console.log("The success message is: "+ele.getText())
  }
  else{
    throw "Account is not created successfully!!!"
  }
});
