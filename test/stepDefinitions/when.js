const { When} = require('@cucumber/cucumber')
import sfLoginPage from '../pageobjects/login';
import sfLandingPage from '../pageobjects/landingPage';
import utl   from '../../utilities/common-utilities';


When(/^I click the "([^"]*)" object on "([^"]*)" page$/, function (arg1,arg2) {
    utl.selectFunctionWithParameters(arg1,arg2);
});

When(/^I create a "([^"]*)" account on "([^"]*)" page$/, function (arg1,arg2) {
    utl.selectFunctionWithParameters(arg1,arg2);
});

When(/^I login to SalesForce as "([^"]*)" user$/,  function (arg1) {
     sfLoginPage.login(arg1);
});

When(/^I log out of SalesForce$/,  function () {
    sfLoginPage.logout();
});

