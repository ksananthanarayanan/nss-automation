Feature: New Service Stack SalesForce Automation POC


    Background:

        Given I am on the SalesForce login page

    Scenario: Performing a search operation
        When I login to SalesForce as "System_Administrator" user
        And  I click the "Accounts" object on "Landing" page
        And I create a "Consumer" account on "Accounts" page
        Then The account should be created successfully
        #And I log out of SalesForce